/****** VARIABLES DEFINITION ********/

//Project
variable "CONF_FILES_PATH"   { type = string }

//AWS
variable "aws_region"        { type = string }
variable "aws_access_key"    { type = string }
variable "aws_secret_key"    { type = string }
variable "aws_ecr"           { type = string }
variable "aws_ecr_repo"      { type = string }
variable "aws_ami"           { type = string }
variable "aws_instance_type" { type = string }
variable "vpc_cidr_block"    { type = string  }
variable "sub_cidr_block"    { type = list(string) }
variable "aws_azs"           { type = list(string) }