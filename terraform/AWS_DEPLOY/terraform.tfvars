/********* VARIABLES VALUES *********/

//Project
CONF_FILES_PATH   =  "conf_files"

//AWS
aws_access_key   =  "<YOUR-AWS-ACCESS-KEY>"
aws_secret_key   =  "<YOUR-AWS-SECRET-ACCESS-KEY>"
aws_ecr          =  "<YOUR-ACCOUNT-ID>.dkr.ecr.us-east-1.amazonaws.com"
aws_region       =  "us-east-1"

aws_ami           =  "ami-04902260ca3d33422"
aws_instance_type =  "t2.micro"

aws_ecr_repo      =  "flask-app-repo"
vpc_cidr_block    =  "10.0.0.0/16"
sub_cidr_block    =  ["10.0.1.0/24", "10.0.2.0/24"]
aws_azs           =  ["us-east-1a", "us-east-1b", "us-east-1c"]
 